libdata-page-perl (2.03-3) UNRELEASED; urgency=medium

  * Update standards version to 4.6.2, no changes needed.

 -- Debian Janitor <janitor@jelmer.uk>  Wed, 21 Dec 2022 20:05:12 -0000

libdata-page-perl (2.03-2) unstable; urgency=medium

  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.

 -- Debian Janitor <janitor@jelmer.uk>  Sat, 28 May 2022 11:43:49 +0100

libdata-page-perl (2.03-1) unstable; urgency=medium

  [ gregor herrmann ]
  * Update debian/upstream/metadata.
  * Update upstream contact.
  * Install new upstream CONTRIBUTING file.

  [ Nick Morrott ]
  * New upstream version 2.03
  * d/control:
    - Declare compliance with Debian Policy 4.4.1 (no changes)
    - Bump debhelper compatibility version to 12
    - Refresh build dependencies
    - Add Rules-Rules-Root field
    - Uploaders: Use my new Debian email address
    - Reformat (cme fix)
  * d/copyright:
    - Refresh Debian Files stanza
  * d/watch:
    - Migrate to version 4 watch file format

 -- Nick Morrott <nickm@debian.org>  Sun, 17 Nov 2019 16:22:26 +0000

libdata-page-perl (2.02-2) unstable; urgency=medium

  * Take over for the Debian Perl Group, as agreed with Bart
  * d/changelog:
    - remove trailing whitespace
  * d/compat:
    - bump debhelper compatibility level to 11
  * d/control:
    - declare compliance with Debian Policy 4.3.0
    - bump debhelper compatibility level to 11
    - add Vcs-Git and Vcs-Browser fields
    - refresh (build-)dependencies
    - add autopkgtest support
    - refresh short/long package descriptions
  * d/copyright:
    - replace with copyright details in 1.0 format
  * d/rules:
    - refresh with default dh target
  * d/s/format:
    - specify quilt format
  * d/u/metadata:
    - add upstream metadata
  * d/watch:
    - migrate to track metacpan releases

 -- Nick Morrott <knowledgejunkie@gmail.com>  Fri, 22 Feb 2019 15:07:21 +0000

libdata-page-perl (2.02-1) unstable; urgency=low

  * New upstream release.
  * debian/control: Fixed debhelper-but-no-misc-depends.

 -- Bart Martens <bartm@debian.org>  Sat, 12 Dec 2009 16:20:53 +0100

libdata-page-perl (2.01-1) unstable; urgency=low

  * New upstream release.

 -- Bart Martens <bartm@debian.org>  Sun, 04 Jan 2009 14:41:33 +0100

libdata-page-perl (2.00-4) unstable; urgency=low

  * debian/control: Standards-Version, Homepage.
  * debian/control: Changed my e-mail address.

 -- Bart Martens <bartm@debian.org>  Mon, 30 Jun 2008 10:42:09 +0200

libdata-page-perl (2.00-3) unstable; urgency=low

  * New maintainer, as agreed with Stephen.
  * debian/*: Use cdbs.
  * debian/control: Added libtest-pod-coverage-perl (>= 1.04) to
    Build-Depends-Indep for "make test".
  * debian/watch: Updated to version 3.

 -- Bart Martens <bartm@knars.be>  Sat, 18 Nov 2006 15:12:14 +0100

libdata-page-perl (2.00-2) unstable; urgency=low

  * Moved debhelper to Build-Depends
  * Switched to my debian.org email address throughout

 -- Stephen Quinney <sjq@debian.org>  Wed, 21 Jun 2006 18:31:51 +0100

libdata-page-perl (2.00-1) unstable; urgency=low

  * New upstream release - new functionality and improved documentation.
  * Added new dependency on libclass-accessor-chained-perl.

 -- Stephen Quinney <sjq@debian.org>  Sun,  5 Dec 2004 16:37:25 +0000

libdata-page-perl (1.03-1) unstable; urgency=low

  * New upstream release - small new feature and some more build tests
  * Added build dependencies on libtest-exception-perl and libtest-pod-perl

 -- Stephen Quinney <sjq@debian.org>  Sat, 31 Jul 2004 08:45:03 +0100

libdata-page-perl (1.02-1) unstable; urgency=low

  * New upstream release

 -- Stephen Quinney <sjq@debian.org>  Mon, 12 Jul 2004 15:22:53 +0100

libdata-page-perl (1.01-1) unstable; urgency=low

  * Initial Release, closes: #252963.

 -- Stephen Quinney <sjq@debian.org>  Tue, 29 Jun 2004 18:19:31 +0100
